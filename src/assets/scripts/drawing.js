const cl = q => console.log(q)

var drawing = {

	// params 
	container: false,
	imgLeft: false,
	imgRight: false,
	drag: false,
	dragColor: false,
	titleLeft: false,
	paragrahLeft: false,
	textBottomLeft: false,
	LinkLeft: false,
	titleRight: false,
	paragrahRight: false,
	textBottomRight: false,
	LinkRight: false,

	// init plugins
	init: function(q){

		this.paramsCheck(q)
		this.initSlide()
		this.addImages()
		this.editMove()

	},
	
	// check params 
	paramsCheck: function(q) {

		q.container ? this.container = q.container : false
		q.imgLeft ? this.imgLeft = q.imgLeft : false
		q.imgRight ? this.imgRight = q.imgRight : false
		q.drag ? this.drag = q.drag : false
		q.dragColor ? this.dragColor = q.dragColor : false
		// left
		q.titleLeft ? this.titleLeft = q.titleLeft : false
		q.paragrahLeft ? this.paragrahLeft = q.paragrahLeft : false
		q.textBottomLeft ? this.textBottomLeft = q.textBottomLeft : false
		q.LinkLeft ? this.LinkLeft = q.LinkLeft : false
		// right
		q.titleRight ? this.titleRight = q.titleRight : false
		q.paragrahRight ? this.paragrahRight = q.paragrahRight : false
		q.textBottomRight ? this.textBottomRight = q.textBottomRight : false
		q.LinkRight ? this.LinkRight = q.LinkRight : false

	},

	// init slider
	initSlide: function() {
		var link = this.LinkRight;
		let maxWidth = $(window).innerWidth()
		$(this.container).append(`
			<div class="drawing-container__item" id=""></div>
			<div class="drawing-container__move" id=""></div>
		`)
		$('.drawing-container__move').slider({
			range: "min",
			value: 50,
			min: 1,
			max: 100,
			slide: function( event, ui ) {

				// event.preventDefault();
				// console.log($(event.toElement)[0].attributes.id.nodeValue)
				if($(event.toElement)[0].attributes.id){
					if($(event.toElement)[0].attributes.id.nodeValue == "test1"){
						event.preventDefault()
						var link = $(event.toElement)[0].attributes.id.nodeValue
						// $(`#${link}`).attr('href')
						window.location.href = $(`#${link}`).attr('href');
					}
				}
			}
		})
		$('.drawing-container__move').eq(0).find('.ui-slider-range').append(`
		<div class="drawing-container__move--img-left" style="height:100%;width: ${maxWidth}px;position: absolute;top:0;left:0"></div>
		`)
	if(this.titleLeft || this.paragrahLeft || this.LinkLeft) {
		$('.drawing-container__move').eq(0).find('.ui-slider-range').append(`
		<div style="position: absolute;left:50px;top: 0;bottom: 0;color: #fff;display: flex;align-items: center;flex-direction: column;justify-content: center;">
			<div class="drawing-left home__block__right" style="width:300px;height:180px">
			</div>
		</div>
		`)
	if(this.titleLeft) {
		$('.drawing-container__move').eq(0).find('.drawing-left').append(`
			<p style="font-size:26px;font-family: ProximaNova-Bold, sans-serif">${this.titleLeft}</p>
		`)	
	}
	if(this.paragrahLeft) {
		$('.drawing-container__move').eq(0).find('.drawing-left').append(`${this.paragrahLeft}`)
	}
	if(this.LinkLeft) {
		$('.drawing-container__move').eq(0).find('.drawing-left').append(`
			<div class="home__block__right__button">
				<a id="test1" href="${this.LinkLeft}" class="btn-general btn-orange" style="pointer-events:initial; margin:0">${this.textBottomRight}</a>
			</div>
		`)
	}
	}
	if(this.titleRight || this.paragrahRight || this.LinkRight) {
		$('.drawing-container__item').append(`
			<div style="position: absolute;top: 0;bottom: 0;right:150px;color: #fff;display: flex;align-items: center;flex-direction: column;justify-content: center;">
				<div class="drawing-right home__block__right" style="">
				</div>
			</div>
		`)
	
		if(this.titleRight) {
			$('.drawing-container__item').find('.drawing-right').append(`${this.titleRight}`)		
		}
		if(this.paragrahRight) {
			$('.drawing-container__item').find('.drawing-right').append(`${this.paragrahRight}`)		
		}
		if(this.LinkRight) {
			$('.drawing-container__item').find('.drawing-right').append(`
				<a href="${this.LinkRight}" class="btn-general btn-blue hvr-grow-shadow" style="color:#fff;font-size:12px; margin:0">${this.textBottomLeft}</a>
			`)		
		}
	}		
	},

	// Load images and append divs
	addImages: function() {

		$('.drawing-container__item').css({
			'background-image': `url(${this.imgLeft})`
		})
		$('.drawing-container__move--img-left').css({
			'background-image': `url(${this.imgRight})`
		})

	},

	editMove: function() {
		let item = `<div class="hr-figuras">
		<span class="hr__arrows">
			<span class="hr__arrow__text">
				MUEVA A LOS LADOS
			</span>
			<span class="hr__arrow hr__arrow--left">
				<span class="hr__arrow__triangle--1"></span>
				<span class="hr__arrow__triangle--2"></span>
				<span class="hr__arrow__triangle--3"></span>
				<span class="hr__arrow__triangle--4"></span>
				<span class="hr__arrow__triangle--5"></span>
			</span>
			<span class="hr__arrow hr__arrow--right">
				<span class="hr__arrow__triangle--1"></span>
				<span class="hr__arrow__triangle--2"></span>
				<span class="hr__arrow__triangle--3"></span>
				<span class="hr__arrow__triangle--4"></span>
				<span class="hr__arrow__triangle--5"></span>
			</span>
		</span>
	</div>`
		$('span.ui-slider-handle.ui-corner-all.ui-state-default').append(item)
	}


	
}