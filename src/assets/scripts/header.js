$(function(){
	
	//clone div's desktop to menu resposnsive 
	$('.header-list').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('header-list');
	$('.header-logo').clone().prependTo('.menu-sidebar-cnt').removeClass('header-logo').addClass('responsive-logo');
	$('.header-languages-select').clone().appendTo('.menu-sidebar-cnt');
	
	//events: menu burguer
	function cerrar_nav() {
		$('.header__menu').removeClass('active');
		$('.closeMenu').removeClass('active');
		$('.menu').removeClass('active');
		$('.menu__overlay').removeClass('active');
		$('body').removeClass('active');
	}

	function abrir_nav(){
		$('.header__menu').addClass('active');
		$('.menu__close').addClass('active');
		$('.menu').addClass('active');
		$('.menu__overlay').addClass('active');
		$('body').addClass('active');
	}

	$('.closeMenu, .closeOverlay').click(function(event) {
		cerrar_nav();
	});	

	$('.openMenu').click(function(event) {
		abrir_nav()
	});

	// +++ fancy +++
	function abrir_fancy(){
		$('.fancy__overlay').addClass('active');
		$('.fancy__block').addClass('active');
		$('body').addClass('active');
	}

	function cerrar_fancy(){
		event.preventDefault();
		$('.fancy__overlay').removeClass('active');
		$('.fancy__block').removeClass('active');
		$('body').removeClass('active');
	}

	$('.openFancy').click(function(event) {
		event.preventDefault();
		abrir_fancy();
	});

	$('.closeFancy, .closeOverlayFancy').click(function(event) {
		cerrar_fancy()
	});
	// --- end ---	

	//detectando tablet, celular o ipad
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};
	
	// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		// tasks to do if it is a Mobile Device
		function readDeviceOrientation() {
			if (Math.abs(window.orientation) === 90) {
				// Landscape
				cerrar_nav();
				cerrar_fancy();
			} else {
				// Portrait
				cerrar_nav();
				cerrar_fancy();
			}
		}
		window.onorientationchange = readDeviceOrientation;
	}else{
		$(window).resize(function() {
			var estadomenu = $('.menu').width();
			if(estadomenu != 0){
				cerrar_nav();
				cerrar_fancy();
			}
		});
	}

	// Ancla scroll - AGREGAR CLASE DEL ENLACE
	// $('.linkScroll a').click(function() {
	// 	cerrar_nav();
	// 	if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
	// 		var $target = $(this.hash);
	// 		$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
	// 		if ($target.length) {
	// 		var targetOffset = $target.offset().top-50;
	// 		$('html,body').animate({scrollTop: targetOffset}, 1000);
	// 		return false;
	// 		}
	// 	}
	// });

	// header scroll
	var altoScroll = 0
	$(window).scroll(function() {
		altoScroll = $(window).scrollTop();
		if (altoScroll > 0) {
			$('.header').addClass('active');
		}else{
			$('.header').removeClass('active');
		};
	});
	});
	